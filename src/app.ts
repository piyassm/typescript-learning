console.log("test");

const button = document.querySelector("button")! as HTMLButtonElement;
const input1 = document.getElementById("num1")! as HTMLInputElement;
const input2 = document.getElementById("num2")! as HTMLInputElement;

type Combine = number | string;
type ConversionType = "as-number" | "as-text";

function add(
  num1: Combine,
  num2: Combine,
  resultConversion: ConversionType,
  showResult: boolean = false,
  phrase: string = "",
  cb: Function = () => {}
): void {
  let result: Combine;
  if (
    (typeof num1 === "number" && typeof num2 === "number") ||
    (resultConversion === "as-number" && !isNaN(+num1) && !isNaN(+num2))
  ) {
    result = +num1 + +num2;
  } else {
    result = num1.toString() + num2.toString();
  }
  if (showResult) {
    console.log(phrase + result);
  }
  cb(result);
}

let CombineFunction: (
  a: Combine,
  b: Combine,
  c: ConversionType,
  d: boolean,
  e: string
) => void;
CombineFunction = add;

let CombineFunction2: Function;
CombineFunction2 = CombineFunction;

let CombineFunction3: (
  a: Combine,
  b: Combine,
  c: ConversionType,
  d: boolean,
  e: string,
  f: (data: Combine) => void
) => void;
CombineFunction3 = add;

button.addEventListener("click", function () {
  CombineFunction(
    +input1.value,
    +input2.value,
    "as-number",
    true,
    "Result is: "
  );
  CombineFunction(input1.value, input2.value, "as-number", true, "Result is: ");
  CombineFunction("I am ", "String", "as-text", true, "Result is: ");
  CombineFunction2("20", "21", "as-text", true, "Result is: ");
  CombineFunction2("20", "21", "as-number", true, "Result is: ");
  CombineFunction3("20", "a", "as-number", true, "Result is: ", (result) => {
    console.log("Callback is: " + result);
    return result;
  });
  // generateError("Test Error", 500);
});

enum Role {
  ADMIN = 99,
}

const person: {
  name: string;
  age: number;
  role: [number, string];
  roleId: number;
  hobbies: string[];
  other: any[];
} = {
  name: "Piya",
  age: 29,
  role: [99, "admin"],
  roleId: Role.ADMIN,
  hobbies: ["Learn Udemy", "Watch Youtube", "Listen Music By IU"],
  other: ["I", "want", 2, "be", "lead", "dev"],
};
person.role.push("test push");
console.log(person.name);

if (person.roleId == Role.ADMIN) {
  console.log("Is Admin!");
}

function generateError(message: string, errorCode: number): never {
  throw { message, errorCode };
}
